photoshareApp.cC({
	name: 'SignInCtrl',
	inject: [
    '$scope',
		'Backand',
		'$cookieStore'
  ],
	init: function() {
    this.$.pageTitle = 'Photoshare - SignIn';
		this.$.Form = {};
		this.appName = 'photoshare';
		console.log(this.$cookieStore.get('token'));
	},
	methods: {
    signIn: function() {
			this.Backand.signin(this.$.Form.username, this.$.Form.password, this.appName)
      .then(
        function (token) {
					this.$cookieStore.put('backand_token', token);
					console.log('sucesso!');
        }.bind(this),
        function (data, status, headers, config) {
					console.log('erro de login');
        }
      );
    }
	}
});
