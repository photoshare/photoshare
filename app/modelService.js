/*
 Arquivo com os Models em uso
 e disponeivels na API do sistema

*/
photoshareApp.service('modelService', ['$resource', function($resource) {

	//Base URL
	this.baseURL = 'https://api.backand.com:8078/1/objects';

	// Params
	this.params = function(model, params) {
		//console.log(model);
		if (params == undefined) params = {};
		p = {};
		if (params.sort == undefined)
			if (this[model].params.sort != undefined)
				p.sort = this[model].params.sort;
			else p.sort = params.sort;
		return p;
	};

	// Actions
	this.get = function(model, id, params) {
		defparams = this.params(model, params);
		if (this[model] == undefined) {
			return {
				error: 'Model Error!'
			};
		}
		if (id!=undefined) defparams.id = id;
		return $resource(this.baseURL+'/api/'+model+'/:id.json')
			.get(defparams).$promise;
	};
	this.save = function(model, data) {
		if (data.id != undefined) id = data.id;
			else id = null;
		return $resource(this.baseURL+'/api/'+model+'/:id.json')
			.save({id:id}, data).$promise;
	};
	this.del = function(model, id) {
		return $resource(this.baseURL+'/api/'+model+'/:id.json')
			.delete({id:id}).$promise;
	};

	// Model Auth ( Login do Usuario )
	this.Auth = {
		res: $resource('/auth.json'),
		auth: function() {
			return this.Auth.res.get().$promise;
		}.bind(this),
	};

	// Model Estado
	this.estado = {
		params: { // parâmetros padrão
			sort: 'nome',
			page: 1,
		},
	};

}]);
