var photoshareApp = angular.module('photoshareApp',
	[
    'ngCookies',
    'backand',
		'classy',
		'ngResource',
		'ngRoute'
	]
);
//Update Angular configuration section
photoshareApp.config(function (BackandProvider) {
  BackandProvider.manageDefaultHeaders();
  BackandProvider.setAnonymousToken('c495491b-0bef-4c1a-9724-7515a4563413');
  BackandProvider.setSignUpToken('802a4291-1f13-475f-a3f5-9e3f8bc21222');
});

photoshareApp.service('APIInterceptor', function($rootScope, UserService) {
    var service = this;

    service.request = function(config) {
        var access_token = $cookieStore.get('backand_token');

        if (access_token) {
            config.headers.authorization = access_token;
        }
        return config;
    };

    service.responseError = function(response) {
        return response;
    };
})
