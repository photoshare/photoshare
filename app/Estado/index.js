photoshareApp.cC({
	name: 'EstadoCtrl',
	inject: [
    '$scope',
		'modelService'
  ],
	init: function() {
    this.$.pageTitle = 'Photoshare - Estados';
    this.load();
	},
	methods: {
    load: function() {
			this.$.estados = this.modelService.get('estado');
    }
	}
});
