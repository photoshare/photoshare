/*
 Arquivo com as rotas em uso pelo ngRoute do AngularJS

*/

photoshareApp.config(function($routeProvider) {
	$routeProvider
	.when('/', {
		controller: 'HomeCtrl',
		templateUrl: '/app/Home/index.html'
	})

	// Login
	.when('/signin', {
		controller: 'SignInCtrl',
		templateUrl: '/app/SignIn/auth.html'
	})

	// Estado
	.when('/estados', {
		controller: 'EstadoCtrl',
		templateUrl: '/app/Estado/index.html'
	})

	// Se não for nenhuma das rotas acima, mostrar mensagem de erro 404 ( não encontrado )
	.otherwise({
		templateUrl: '/app/errors/404.html'
	});

});
